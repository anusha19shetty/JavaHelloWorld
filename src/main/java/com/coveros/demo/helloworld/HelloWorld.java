package com.coveros.demo.helloworld;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HelloWorld {

  public static void main(final String[] args) {
    final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("h:mm:ss a 'on' MMMM d, yyyy'.'");
    final LocalDateTime now = LocalDateTime.now();
    System.out.println("Hello, Child Branch, trying to merge with Master !! The current time is " + dtf.format(now));
    System.out.println("Sample");
    System.out.println("Sample2");
    System.out.println("Sample3");
    System.out.println("Sample4");
    System.out.println("Hello Bangalore");
  }

}
